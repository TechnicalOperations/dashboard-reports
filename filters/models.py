from django.db import models
from datetime import timedelta
from django.utils import timezone

FUNNEL_ORDINAL = [
    'post_rg',
    'ssp_id',
    'dsps',
    'device',
    'imp_type',
    'media_type',
    'ad_size',
    'video_size',
    'country_code',
    'in_view_percentage',
    'segments'
]


class PmpFunnel(models.Model):
    day = models.DateField(default=timezone.now() - timedelta(hours=24))
    deal_id = models.CharField(max_length=255, default='Unknown')
    post_rg = models.BigIntegerField(default=0)
    ssp_id = models.BigIntegerField(default=0)
    dsps = models.BigIntegerField(default=0)
    device = models.BigIntegerField(default=0)
    imp_type = models.BigIntegerField(default=0)
    media_type = models.BigIntegerField(default=0)
    ad_size = models.BigIntegerField(default=0)
    video_size = models.BigIntegerField(default=0)
    country_code = models.BigIntegerField(default=0)
    in_view_percentage = models.BigIntegerField(default=0)
    segments = models.BigIntegerField(default=0)
