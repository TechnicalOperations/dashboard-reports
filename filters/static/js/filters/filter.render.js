function includeCsrfInRequests($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
}

var app = angular.module('funnelApp', []).config(includeCsrfInRequests);

app.controller('funnelController', function($scope, $http) {
    $scope.data = {};
    $scope.search = ""
    $http.post(window.location.pathname, null)
        .then(function(response) {
            $scope.data = response.data;
            $scope.data.coord = [];
            $scope.data.percent = [];
            var value;
            var percent;
            for (var index=0; index < $scope.data.ordinal.length; index++) {
                value = $scope.data.ordinal[index];
                $scope.data.coord.push($scope.data.funnel[value]);
                percent = $scope.data.funnel[value] * 100.0 / $scope.data.funnel[$scope.data.ordinal[0]];
                percent = Math.round(percent * 100) / 100;
                $scope.data.percent.push(percent);
            }
            console.dir(response.data);
            $scope.context = document.getElementById('pmpFunnel').getContext('2d');
            $scope.chart = new Chart($scope.context, {
                type: 'bar',
                data: {
                    labels: $scope.data.ordinal,
                    datasets: [{
                        label: $scope.data.deal_id,
                        backgroundColor: "rgb(0, 0, 255)",
                        borderColor: "rgb(120, 120, 255)",
                        fill: true,
                        data: $scope.data.coord
                    }]
                }
            });
            console.log("Successfully fetched filter JSON");
        }, function(response) {
            console.log("Error getting filter JSON");
        });

    $scope.searchFunction = function() {
        $http.post('/pmp-funnel/search/'+$scope.search+'/', null)
            .then(function(response) {
                console.dir(response.data);
                console.log("Successfully fetched search JSON");
        }, function(response) {
            console.log("Error getting search JSON");
        });
    }
});