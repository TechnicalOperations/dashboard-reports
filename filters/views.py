import pandas
from util import sql
from django.shortcuts import render
from django.http import JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta
from filters import models as filter_models


global updated
global locked
global deals
updated = datetime.utcnow() - timedelta(hours=1)
locked = False


@csrf_exempt
def index(request, deal_id=None):
    if request.method == 'GET':
        return render(request, 'filters/index.html')
    elif request.method == 'POST' and deal_id is not None:
        object = filter_models.PmpFunnel.objects.filter(deal_id=deal_id)[:1].get()
        data = {}
        for i in filter_models.FUNNEL_ORDINAL:
            data[i] = getattr(object, i)
        data = {
            'ordinal': filter_models.FUNNEL_ORDINAL,
            'funnel': data,
            'deal_id': deal_id
        }
        return JsonResponse(data, json_dumps_params={'indent': 4, 'sort_keys': True})
    else:
        raise Http404()


def text_completion(request, text=''):
    # if request.method == 'GET':
    #     raise Http404()
    global updated
    global locked
    global deals
    if updated < datetime.utcnow() - timedelta(hours=1) and not locked:
        locked = True
        updated = datetime.utcnow()
        con = sql.connect()
        q = """
            SELECT external_deal_id, description
            FROM rx.deal
        """.format(text.upper())
        deals = pandas.read_sql_query(q, con)
        deals = deals.rename(columns={i: i.lower() for i in deals.columns})
        sql.close()
        locked = False
    text = text.upper()
    out = deals[deals['external_deal_id'].str.contains(text) | deals['description'].apply(lambda x: text in x.upper())]
    out = out.to_dict(orient='records')
    return JsonResponse(out, json_dumps_params={'indent': 4, 'sort_keys': True}, safe=False)
