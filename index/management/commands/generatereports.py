import pandas
from . import queries
from util import sql
from index.models import OpenManageCtr, PmpPerformance, PmpDspNobidBcat
from django.core.management.base import BaseCommand
pandas.set_option('display.width', 5000)


class Command(BaseCommand):

    REPORTS = (
        # (queries.OPEN_MANAGE_CTR, OpenManageCtr,),
        (queries.DEAL_PERFORMANCE, PmpPerformance,),
        # (queries.PMP_DSP_NOBID_BCAT, PmpDspNobidBcat),
    )

    def save_data(self, data, model):
        data = data.rename(columns={i: i.lower() for i in list(data.columns)})
        data = data.to_dict(orient='records')
        items = []
        for item in data:
            items.append(model(**item))
        model.objects.bulk_create(items)

    def handle(self, *args, **options):
        con = sql.connect()
        for query, model in Command.REPORTS:
            # model.objects.all().delete()
            data = pandas.read_sql_query(query, con)
            data = queries.FUNCTIONS[query](data)
            self.save_data(data, model)
        con.close()
