import json
import pandas
import datetime
from copy import deepcopy

end = datetime.datetime.now()
end = end - datetime.timedelta(hours=end.hour)
start = end - datetime.timedelta(days=1)

def sorted_str(x, delimiter=','):
    x = list(set(x))
    x.sort()
    return delimiter.join(x)


def flatten(data, column):
    cols = list(data.columns)
    data = data.to_dict(orient='records')
    out = []
    for item in data:
        if len(item[column]) == 0:
            item[column] = ''
            out.append(item)
            continue
        for i in list(item[column]):
            item = deepcopy(item)
            item[column] = i
            out.append(item)
    return pandas.DataFrame(out)[cols]

DEAL_PERFORMANCE = """
	WITH imps AS (
	    SELECT DATE_TRUNC('DAY', A.event_date) AS imp_date,
	            A.deal_id,
	            A.dsp_seat AS seat,
	            A.ssp_id,
	            A.dsp_id,
	            SUM(impression_win) AS impressions,
	            SUM(revenue) AS revenue,
	            SUM(revenue - cost) AS margin
	    FROM r1_edw.rx.impression_d_est AS A
	    WHERE A.event_date >= '{start}' AND A.event_date < '{end}'
	        AND (A.deal_id != '-' OR (A.dsp_seat = '1058' AND dsp_id = 'turn'))
	    GROUP BY imp_date, A.deal_id, A.dsp_seat, A.ssp_id, A.dsp_id
	),
	bids AS (
	    SELECT DATE_TRUNC('DAY', A.event_time_est) AS date,
	            E.value::varchar AS deal_id,
	            A.ssp_id,
	            A.dsp_id,
	            SUM(IFF(A.request_id IS NOT NULL, A.sample_rate, 0)) AS requests,
	            SUM(IFF(CONTAINS(E.value, B.deal_id) AND E.value IS NOT NULL, A.sample_rate, 0)) AS bid_received,
	            SUM(IFF(NOT(CONTAINS(E.value, B.deal_id)) AND B.deal_id IS NOT NULL, A.sample_rate, 0)) AS lost_other
	    FROM rx.dspbidrequest AS A
	    LEFT JOIN rx.bidresponse AS B ON A.request_id=B.request_id
	                            AND A.dsp_id=B.dsp_id,
	    LATERAL FLATTEN(input=>PARSE_JSON(A.deal_ids), outer=>TRUE) E
	    WHERE A.event_time_est >= '{start}' AND A.event_time_est < '{end}'
	        AND (B.event_time_est >= '{start}' OR B.event_time_est IS NULL)
	        AND (B.event_time_est < '{end}' OR B.event_time_est IS NULL)
	    GROUP BY date, E.value, A.ssp_id, A.dsp_id
	)
	SELECT IFF(A.date IS NULL, B.imp_date, A.date) AS date,
	       A.deal_id,
	       IFF(A.ssp_id IS NULL, B.ssp_id, A.ssp_id) AS ssp_id,
	       IFF(A.dsp_id IS NULL, B.dsp_id, A.dsp_id) AS dsp_id,
	       B.seat,
	       A.requests,
	       A.bid_received,
	       A.lost_other,
	       B.impressions,
	       B.revenue,
	       B.margin
	FROM bids AS A
	FULL OUTER JOIN imps AS B ON A.date=B.imp_date
	                         AND (A.deal_id=B.deal_id OR (A.deal_id IS NULL AND B.deal_id='-'))
	                         AND A.ssp_id=B.ssp_id
	                         AND A.dsp_id=B.dsp_id
	WHERE A.deal_id IS NOT NULL OR B.seat = '1058'
""".format(start=start.strftime('%Y-%m-%d'), end=end.strftime('%Y-%m-%d'))


def deal_performance(data):
    data = data.rename(columns={i: i.lower() for i in list(data.columns)})
    data['deal_id'] = data['deal_id'].fillna('').apply(lambda x: x.strip())
    data['seat'] = data['seat'].fillna('').apply(lambda x: x.strip())
    data = data[((data['deal_id'] != '') & ~data['deal_id'].isnull()) | ((data['seat'] != '') & ~data['seat'].isnull())]
    data['requests'] = data['requests'].fillna(0).astype(int)
    data['bid_received'] = data['bid_received'].fillna(0).astype(int)
    data['lost_other'] = data['lost_other'].fillna(0).astype(int)
    data['impressions'] = data['impressions'].fillna(0).astype(int)
    data['revenue'] = data['revenue'].fillna(0.0).astype(float)
    data['margin'] = data['margin'].fillna(0.0).astype(float)
    data = data.fillna('')
    data = data.groupby(['date', 'deal_id', 'seat', 'ssp_id', 'dsp_id']).sum().reset_index()
    return data


OPEN_MANAGE_CTR = """
    SELECT DATE_TRUNC('DAY', A.event_time_est) AS date,
            A.adomain,
            A.dsp_id,
            A.dsp_seat AS seat,
            SUM(IFF(B.deal_id IS NULL OR B.deal_id='', 0, 1)) AS deal_click_count,
            SUM(IFF(A.deal_id IS NULL OR A.deal_id='', 0, 1)) AS deal_total,
            SUM(IFF(B.request_id IS NULL, 0, 1)) AS open_click_count,
            SUM(IFF(A.deal_id IS NULL OR A.deal_id='', 1, 0)) AS open_total
        FROM rx.impserve AS A
        FULL OUTER JOIN rx.click AS B ON A.request_id=B.request_id
        WHERE A.event_time_est >= '{start}' AND A.event_time_est < '{end}'
        GROUP BY date, A.adomain, A.dsp_id, A.dsp_seat
        HAVING deal_total > 0
""".format(start=start.strftime('%Y-%m-%d'), end=end.strftime('%Y-%m-%d'))


def open_manage_ctr(data):
    data = data.rename(columns={i: i.lower() for i in list(data.columns)})
    data = data[~data['adomain'].isnull() & data['adomain'].apply(lambda x: len(x) > 2)]
    data['adomain'] = data['adomain'].apply(lambda x: sorted_str(json.loads(x)))
    data['seat'] = data['seat'].fillna('')
    data = data.fillna(0)
    data = data.groupby(['date', 'adomain', 'dsp_id', 'seat']).sum().reset_index()
    data['deal_click_rate'] = data.apply(
        lambda x: x['deal_click_count'] * 100.0 / x['deal_total'] if x['deal_total'] > 0 else 0, axis=1
    )
    data['open_click_rate'] = data.apply(
        lambda x: x['open_click_count'] * 100.0 / x['open_total'] if x['open_total'] > 0 else 0, axis=1
    )
    return data


PMP_DSP_NOBID_BCAT = """
    SELECT DATE_TRUNC('DAY', A.event_time_est) AS date,
            A.ssp_id,
            A.dsp_id,
            A.deal_ids,
            A.request_data:bcat AS bcat,
            COUNT(*) AS requests,
            COUNT(B.request_id) AS bids,
            SUM(IFF(CONTAINS(A.deal_ids, B.deal_id), 1, 0)) AS deal_bids
    FROM rx.dspbidrequest AS A
    LEFT JOIN rx.bidresponse AS B ON A.request_id=B.request_id AND A.dsp_id=B.dsp_id
    WHERE A.event_time_est >= '{start}' AND A.event_time_est < '{end}'
    GROUP BY date, A.ssp_id, A.dsp_id, A.deal_ids, bcat
""".format(start=start.strftime('%Y-%m-%d'), end=end.strftime('%Y-%m-%d'))


def pmp_dsp_nobid_bcat(data):
    data = data.rename(columns={i: i.lower() for i in list(data.columns)})
    data['bcat'] = data['bcat'].apply(lambda x: json.loads(x) if type(x) == str else [])
    data = flatten(data, 'bcat')
    data['deal_ids'] = data['deal_ids'].apply(lambda x: json.loads(x) if type(x) == str else [])
    data = flatten(data, 'deal_ids')
    data = data.groupby(['date', 'ssp_id', 'dsp_id', 'deal_ids', 'bcat']).sum().reset_index()
    return data


FUNCTIONS = {
    DEAL_PERFORMANCE: deal_performance,
    OPEN_MANAGE_CTR: open_manage_ctr,
    PMP_DSP_NOBID_BCAT: pmp_dsp_nobid_bcat
}
