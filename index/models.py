from django.db import models


DEVICE_TYPES = (
    ('1', 'Mobile/Tablet'),
    ('2', 'Personal Computer'),
    ('3', 'Connected TV'),
    ('4', 'Phone'),
    ('5', 'Tablet'),
    ('6', 'Connected Device'),
    ('7', 'Set Top Box'),
)

CREATIVE_ATTRS = (
    ('1', 'Audio Ad (Auto-Play)'),
    ('2', 'Audio Ad (User Initiated)'),
    ('3', 'Expandable (Automatic)'),
    ('4', 'Expandable (User Initiated - Click)'),
    ('5', 'Expandable (User Initiated - Rollover)'),
    ('6', 'In-Banner Video Ad (Auto-Play)'),
    ('7', 'In-Banner Video Ad (User Initiated)'),
    ('8', 'Pop (e.g., Over, Under, or Upon Exit)'),
    ('9', 'Provocative or Suggestive Imagery'),
    ('10', 'Shaky, Flashing, Flickering, Extreme Animation, Smileys'),
    ('11', 'Surveys'), ('12', 'Text Only'),
    ('13', 'User Interactive (e.g., Embedded Games)'),
    ('14', 'Windows Dialog or Alert Style'),
    ('15', 'Has Audio On/Off Button'),
    ('16', 'Ad Can be Skipped (e.g., Skip Button on Pre-Roll Video)'),
)


class OpenManageCtr(models.Model):
    date = models.DateField()
    adomain = models.TextField()
    dsp_id = models.CharField(max_length=255, default='', blank=True, null=True)
    seat = models.CharField(max_length=255, blank=True, null=True, default='')
    deal_click_count = models.IntegerField()
    deal_total = models.IntegerField()
    open_click_count = models.IntegerField()
    open_total = models.IntegerField()
    deal_click_rate = models.FloatField()
    open_click_rate = models.FloatField()


class PmpPerformance(models.Model):
    date = models.DateField()
    deal_id = models.CharField(max_length=255, default='None')
    seat = models.CharField(max_length=255, default='-')
    ssp_id = models.CharField(max_length=255, default='', blank=True, null=True)
    dsp_id = models.CharField(max_length=255, default='', blank=True, null=True)
    requests = models.BigIntegerField()
    bid_received = models.IntegerField()
    lost_other = models.IntegerField(default=0)
    impressions = models.IntegerField(default=0)
    revenue = models.FloatField(default=0.0)
    margin = models.FloatField(default=0.0)


class PmpDspNobidBcat(models.Model):
    date = models.DateField()
    ssp_id = models.CharField(max_length=255, default='', blank=True, null=True)
    dsp_id = models.CharField(max_length=255, default='', blank=True, null=True)
    rx_deal_ids = models.CharField(max_length=255, default='', blank=True, null=True)
    bcat = models.CharField(max_length=255, default='', blank=True, null=True)
    requests = models.IntegerField()
    bids = models.IntegerField()
    deal_bids = models.IntegerField()
