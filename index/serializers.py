import pandas
from datetime import datetime, date
from rest_framework import serializers
from index.models import OpenManageCtr, PmpPerformance, PmpDspNobidBcat


class FilterSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        self.filter_field = kwargs.get('filter_by', {})
        if 'filter_by' in kwargs:
            del kwargs['filter_by']
        super(FilterSerializer, self).__init__(*args, **kwargs)

    def to_representation(self, instance):
        meta = type(self).Meta
        out = {field: set() for field in meta.fields}
        if len(self.filter_field) > 0:
            items = meta.model.objects.filter(**self.filter_field).values(*meta.fields)
        else:
            items = meta.model.objects.all().values(*meta.fields)
        for item in items:
            for field in meta.fields:
                out[field].add(item[field])
        out = {k: sorted(list(v)) for k, v in out.items()}
        return out

    class Meta:
        model = None
        # Fields for which the model needs to be filtered by
        fields = None


class OpenManageCtrFilterSerializer(FilterSerializer):

    class Meta:
        model = OpenManageCtr
        fields = ('adomain', 'dsp_id', 'seat')


class PmpPerformanceFilterSerializer(FilterSerializer):

    class Meta:
        model = PmpPerformance
        fields = ('deal_id', 'ssp_id', 'dsp_id')


class PmpBcatPerformanceFilterSerializer(FilterSerializer):

    class Meta:
        model = PmpDspNobidBcat
        fields = ('ssp_id', 'dsp_id', 'rx_deal_ids', 'bcat')


class ChartSerializer(serializers.Serializer):

    CHART_COLORS = [
        'rgba(0, 0, 255, 1)',
        'rgba(0, 255, 0, 1)',
        'rgba(255, 0, 0, 1)',
    ]

    def color_iterator(self):
        return iter(ChartSerializer.CHART_COLORS)

    def to_representation(self, instance):
        out = {
            'datasets': {},
            'labels': []
        }
        meta = type(self).Meta
        data = [
                [(field, getattr(item, field),) for field in meta.fields] +
                [(meta.x_axis[0], getattr(item, meta.x_axis[0]))]
            for item in instance
        ]
        data = [{i[0]: i[1]} for i in data]
        data = pandas.DataFrame(data)
        data = data.groupby(meta.x_axis[0]).sum().reset_index()
        for item in data.to_dict(orient='records'):
            for field in meta.fields:
                if field not in out['datasets']:
                    out['datasets'][field] = []
                out['datasets'][field].append(item.get(field, None))
            label = item.get(meta.x_axis[0], None)
            if type(label) in [datetime, date]:
                label = int(datetime.strptime(label.isoformat(), '%Y-%m-%d').timestamp() * 1000)
            out['labels'].append(label)
        colors = self.color_iterator()
        out = {
            'labels': out['labels'],
            'datasets': [
                {
                    'label': meta.fields[field],
                    'data': item,
                    'backgroundColor': 'rgba(0, 0, 0, 0)',
                    'borderColor': next(colors),
                    'borderWidth': 1
                }
                for field, item in out['datasets'].items()
            ]
        }
        return out

    class Meta:
        model = None
        # Fields need to be a dict in the format {field_name: display_name}
        fields = None
        # x_axis is a tuple for the field which will be used as an x-axis (field_name, display_name,)
        # the x_axis is to be excluded from the fields attribute
        x_axis = None


class OpenManageCtrSerializer(ChartSerializer):

    class Meta:
        model = OpenManageCtr
        fields = {'deal_click_rate': 'Deal Click Rate', 'open_click_rate': ' Open Click Rate'}
        x_axis = ('date', 'Date',)


class PmpPerformanceSerializer(ChartSerializer):

    class Meta:
        model = PmpPerformance
        fields = {
            'requests': 'Requests Sent',
            'bid_received': 'Bids Received',
            'lost_other': 'Lost To Other Deal'
        }
        x_axis = ('date', 'Date',)


class PmpBcatPerformanceSerializer(ChartSerializer):

    class Meta:
        model = PmpDspNobidBcat
        fields = {'requests': 'Requests', 'bids': 'Bids', 'deal_bids': 'Deal Bids'}
        x_axis = ('date', 'Date',)
