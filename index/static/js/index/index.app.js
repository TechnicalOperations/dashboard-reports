function includeCsrfInRequests($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
}

var app = angular.module('pmpApp', []).config(includeCsrfInRequests);

app.controller('pmpController', function($scope, $http) {
    $scope.context = document.getElementById("graph").getContext("2d");
    $scope.data = {};
    $scope.selected = {};

//    $scope.changeFilter = function(item) {
    $http.post(window.location.pathname+"filters/", $scope.selected)
        .then(function(response) {
            $scope.filterOptions = response.data;
            console.log("Successfully fetched filter JSON");
        }, function(response) {
            console.log("Error getting filter JSON");
        });
//    }
//
//    $scope.changeFilter();

    $scope.clearSelected = function() {
        $scope.selected = {}
//        $scope.changeFilter();
    }

    $scope.plotNew = function(data) {
        $scope.chart = new Chart($scope.context, {
            type: 'line',
            data: {
                labels: data.labels,
                datasets: data.datasets
            },
            options: {
                legend: {
                    position: 'left'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    }]
                }
            }
        });
    }

    $scope.plotData = function() {
        $http.post(window.location.pathname+'get/', $scope.selected)
            .then(function(response) {
                $scope.data = response.data;
                $scope.plotNew($scope.data);
                console.log("Successfully retrieved data")
            }, function(response) {
                console.log("Error when retrieving data")
            });
        return $scope.data;
    }

});
