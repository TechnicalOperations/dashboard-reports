import json
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from index.models import OpenManageCtr
from index.serializers import OpenManageCtrSerializer, OpenManageCtrFilterSerializer
from index.serializers import PmpPerformanceSerializer, PmpPerformanceFilterSerializer
from index.serializers import PmpBcatPerformanceSerializer, PmpBcatPerformanceFilterSerializer


# Create your views here.
class Index(APIView):

    def get(self, request):
        return render(request, 'index/index.html')

    def post(self, request):
        data = OpenManageCtr.objects.filter(adomain='ibm.com', seat__in=['', None], dsp_id='mediamathvideo')
        serializer = OpenManageCtrSerializer(data, many=True)
        return Response(serializer.data)


class Generic(APIView):

    def get(self, request):
        return render(request, 'index/generic.html')

    def post(self, request):
        data = OpenManageCtr.objects.filter(adomain='ibm.com', seat__in=['', None], dsp_id='mediamathvideo')
        serializer = OpenManageCtrSerializer(data, many=True)
        return Response(serializer.data)


FILTERS = {
    'managed-open-ctr': (OpenManageCtrFilterSerializer, OpenManageCtrSerializer,),
    'pmp-performance': (PmpPerformanceFilterSerializer, PmpPerformanceSerializer,),
    'pmp-bcat-performance': (PmpBcatPerformanceFilterSerializer, PmpBcatPerformanceSerializer,)
}

class FilterView(APIView):

    def post(self, request, class_view=None):
        filters = {'filter_by': request.data if type(request.data) == dict else {}}
        filter = FILTERS[class_view][0]([], **filters)
        return Response(filter.data)


class ChartView(APIView):

    def post(self, request, class_view=None):
        filters = json.loads(request.body)
        # Get all the data
        data = FILTERS[class_view][1].Meta.model.objects.filter(**filters)
        # then serialize it
        chart = FILTERS[class_view][1](data)
        return Response(chart.data)
