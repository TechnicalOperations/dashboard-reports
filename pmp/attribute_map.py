
ATTRIBUTES = {}

ATTRIBUTES['api'] = {
    '1': 'VPAID 1.0',
    '2': 'VPAID 2.0',
    '3': 'MRAID-1',
    '4': 'ORMMA',
    '5': 'MRAID-2'
}

ATTRIBUTES['battr'] = {
    '1': 'Audio Ad (Auto-Play)',
    '2': 'Audio Ad (User Initiated)',
    '3': 'Expandable (Automatic)',
    '4': 'Expandable (User Initiated - Click)',
    '5': 'Expandable (User Initiated - Rollover)',
    '6': 'In-Banner Video Ad (Auto-Play)',
    '7': 'In-Banner Video Ad (User Initiated)',
    '8': 'Pop (e.g., Over, Under, or Upon Exit)',
    '9': 'Provocative or Suggestive Imagery',
    '10': 'Shaky, Flashing, Flickering, Extreme Animation, Smileys',
    '11': 'Surveys', '12': 'Text Only',
    '13': 'User Interactive (e.g., Embedded Games)',
    '14': 'Windows Dialog or Alert Style',
    '15': 'Has Audio On/Off Button',
    '16': 'Ad Can be Skipped (e.g., Skip Button on Pre-Roll Video)'
}

ATTRIBUTES['pos'] = {
    '0': 'Unknown',
    '1': 'Above the Fold',
    '2': 'DEPRECATED - May or may not be initially visible depending on screen size/resolution.',
    '3': 'Below the Fold',
    '4': 'Header',
    '5': 'Footer',
    '6': 'Sidebar',
    '7': 'Full Screen'
}

ATTRIBUTES['protocol'] = {
    '1': 'VAST 1.0',
    '2': 'VAST 2.0',
    '3': 'VAST 3.0',
    '4': 'VAST 1.0 Wrapper',
    '5': 'VAST 2.0 Wrapper',
    '6': 'VAST 3.0 Wrapper'
}

INVERT = {
    k: {v1: k1 for k1, v1 in v.items()}
    for k, v in ATTRIBUTES.items()
}
