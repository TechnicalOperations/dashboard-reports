import pandas
import numpy as np
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from pmp import models
pandas.set_option('display.width', 5000)


def compare(summary, data):
    index = list(data.index)
    for item in index:
        summary[item] = {
            'bid': [],
            'nobid': []
        }
        row = data.loc[item]
        values = list(set([i[0] for i in list(row.index)]))
        for value in values:
            if np.isnan(row[value]['requests']):
                continue
            if row[value]['deal_bids'] == 0 and row[value]['requests'] > 0:
                summary[item]['nobid'].append(int(value) if type(value) == float else value)
            elif row[value]['deal_bids'] > 0 and row[value]['requests'] < row[value]['total_requests']:
                summary[item]['bid'].append(int(value) if type(value) == float else value)
        if len(summary[item]['nobid']) == 0 and len(summary[item]['bid']) == 0:
            del summary[item]
    return data


class Command(BaseCommand):

    VIDEO_COLUMNS = [
        ('api', models.VideoApi,),
        ('battr', models.VideoBattr,),
        ('size', models.VideoSize,),
        ('maxduration', models.VideoMaxDuration,),
        ('minduration', models.VideoMinDuration,),
        ('mime', models.VideoMimes,),
        ('pos', models.VideoPos,),
        ('protocol', models.VideoProtocols,)
    ]

    BANNER_COLUMNS = [
        ('api', models.BannerApi,),
        ('battr', models.BannerBattr,),
        ('size', models.BannerSize,),
        ('mime', models.BannerMimes,),
        ('pos', models.BannerPos,),
    ]

    COMMON = (
        'dsp_id',
        'deal_id',
        'total_requests',
        'requests',
        'deal_bids',
    )

    def handle(self, *args, **options):
        start = datetime.now() - timedelta(days=100)
        models.VideoBidSummary.objects.all().delete()
        summary = {}
        for col, Model in Command.VIDEO_COLUMNS:
            items = Model.objects.filter(date__gte=start)
            data = []
            for item in items:
                out = {
                    i: getattr(item, i)
                    for i in Command.COMMON
                }
                out[col] = getattr(item, col)
                data.append(out)
            data = pandas.DataFrame(data)
            if len(data) == 0:
                continue
            data = data.groupby(['deal_id', 'dsp_id', col]).sum().reset_index()
            data = pandas.pivot_table(data, columns=[col], index=['deal_id', 'dsp_id'], fill_value=np.nan, aggfunc=np.sum)
            data.columns = data.columns.swaplevel(0, 1)
            cols = list(data.columns)
            cols.sort(key=lambda x: x[0])
            data = data[cols]
            item = {}
            data.groupby(level=0).apply(lambda x: compare(item, x))
            for k, v in item.items():
                if k not in summary:
                    summary[k] = {}
                    summary[k]['bid'] = {}
                    summary[k]['nobid'] = {}
                summary[k]['bid'][col] = ','.join([str(i) for i in v['bid']])
                summary[k]['nobid'][col] = ','.join([str(i) for i in v['nobid']])
        summary = [
            {**{'deal_id': ddsp[0], 'dsp_id': ddsp[1], 'deal_bid': nb=='bid'}, **cols}
            for ddsp, items in summary.items()
            for nb, cols in items.items()
        ]
        summary = [models.VideoBidSummary(**i) for i in summary]
        models.VideoBidSummary.objects.bulk_create(summary)

        models.BannerBidSummary.objects.all().delete()
        summary = {}
        for col, Model in Command.BANNER_COLUMNS:
            items = Model.objects.filter(date__gte=start)
            data = []
            for item in items:
                out = {
                    i: getattr(item, i)
                    for i in Command.COMMON
                }
                out[col] = getattr(item, col)
                data.append(out)
            data = pandas.DataFrame(data)
            if len(data) == 0:
                continue
            data = data.groupby(['deal_id', 'dsp_id', col]).sum().reset_index()
            data = pandas.pivot_table(data, columns=[col], index=['deal_id', 'dsp_id'], fill_value=np.nan, aggfunc=np.sum)
            data.columns = data.columns.swaplevel(0, 1)
            cols = list(data.columns)
            cols.sort(key=lambda x: x[0])
            data = data[cols]
            item = {}
            data.groupby(level=0).apply(lambda x: compare(item, x))
            for k, v in item.items():
                if k not in summary:
                    summary[k] = {}
                    summary[k]['bid'] = {}
                    summary[k]['nobid'] = {}
                summary[k]['bid'][col] = ','.join([str(i) for i in v['bid']])
                summary[k]['nobid'][col] = ','.join([str(i) for i in v['nobid']])
        summary = [
            {**{'deal_id': ddsp[0], 'dsp_id': ddsp[1], 'deal_bid': nb=='bid'}, **cols}
            for ddsp, items in summary.items()
            for nb, cols in items.items()
        ]
        summary = [models.BannerBidSummary(**i) for i in summary]
        models.BannerBidSummary.objects.bulk_create(summary)
