import pandas
from util import sql
from pmp.models import Deals
from django.core.management.base import BaseCommand
pandas.set_option('display.width', 5000)


class Command(BaseCommand):

    def handle(self, *args, **options):
        con = sql.connect()
        data = pandas.read_sql_query(
            "SELECT external_deal_id AS deal_id, description FROM rx.deal",
            con
        )
        data = data.rename(columns={i: i.lower() for i in data.columns})
        data = data.groupby(['deal_id', 'description']).size().reset_index()
        del data[0]
        sql.close()
        data = data.to_dict(orient='records')
        data = [Deals(**i) for i in data]
        Deals.objects.bulk_create(data)
