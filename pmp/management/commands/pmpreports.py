import os
import json
import pandas
from .queries import video, banner
from util import sql
from django.db import transaction
import datetime
import time
from django.core.management.base import BaseCommand
pandas.set_option('display.width', 5000)


class Command(BaseCommand):

    def handle(self, *args, **options):
        con = sql.connect()
        for query, model, t in video.QUERIES:
            # model.objects.all().delete()
            odata = model.objects.all()
            odata = [
                {col.name: getattr(i, col.name) for col in model._meta.get_fields() if col.name != 'id'}
                for i in odata
            ]
            odata = pandas.DataFrame(odata)
            model.objects.all().delete()
            data = pandas.read_sql_query(query, con)
            data = data.rename(columns={i: i.lower() for i in data.columns})
            cols = list(data.columns)
            cols.remove('requests')
            cols.remove('had_deal_response')
            cols.remove('deal_bids')
            col = [i for i in cols if i not in ['dsp_id', 'ssp_id', 'deal_id', 'date']][0]
            try:
                data[col] = data[col].apply(lambda x: [] if type(x) != str else json.loads(x))
                data[col] = data[col].apply(lambda x: tuple(sorted(list(set([str(i) for i in x])))))
                data[col] = data[col].apply(lambda x: ','.join(x))
            except:
                print("Failed on {0}".format(col))
            data['date'] = data['date'].apply(lambda x: pandas.Timestamp(x))
            if len(odata) > 0:
                odata['date'] = odata['date'].apply(lambda x: pandas.Timestamp(x))
                data = pandas.concat([data, odata])
            data = data.groupby(cols).sum().reset_index()
            data.to_csv(os.path.dirname(os.path.realpath(__file__))+"/tmp/video/{0}.csv".format(col), sep=',', index=False)
            # data = data.to_dict(orient='records')
            # items = []
            # for i in range(0, len(data), 10000):
            #     for item in data[i:i + 10000]:
            #         items.append(model(**item))
            #     model.objects.bulk_create(items)
            #     time.sleep(3)
            print("Updated model")
        for query, model, t in banner.QUERIES:
            # model.objects.all().delete()
            odata = model.objects.all()
            odata = [
                {col.name: getattr(i, col.name) for col in model._meta.get_fields() if col.name != 'id'}
                for i in odata
            ]
            odata = pandas.DataFrame(odata)
            model.objects.all().delete()
            data = pandas.read_sql_query(query, con)
            data = data.rename(columns={i: i.lower() for i in data.columns})
            cols = list(data.columns)
            cols.remove('requests')
            cols.remove('had_deal_response')
            cols.remove('deal_bids')
            col = [i for i in cols if i not in ['dsp_id', 'ssp_id', 'deal_id', 'date']][0]
            try:
                data[col] = data[col].apply(lambda x: [] if type(x) != str else json.loads(x))
                data[col] = data[col].apply(lambda x: tuple(sorted(list(set([str(i) for i in x])))))
                data[col] = data[col].apply(lambda x: ','.join(x))
            except:
                pass
            data['date'] = data['date'].apply(lambda x: pandas.Timestamp(x))
            if len(odata) > 0:
                odata['date'] = odata['date'].apply(lambda x: pandas.Timestamp(x))
            data = pandas.concat([data, odata])
            data = data.groupby(cols).sum().reset_index()
            data.to_csv(os.path.dirname(os.path.realpath(__file__))+"/tmp/banner/{0}.csv".format(col), sep=',', index=False)
            # data = data.to_dict(orient='records')
            # items = []
            # for i in range(0, len(data), 10000):
            #     for item in data[i:i + 10000]:
            #         items.append(model(**item))
            #     model.objects.bulk_create(items)
            #     time.sleep(3)
            print("Updated model: {0}".format(col))
        sql.close()
        for query, model, t in video.QUERIES:
            # model.objects.all().delete()
            cols = [i.name for i in list(model._meta.get_fields())]
            cols.remove('requests')
            cols.remove('had_deal_response')
            cols.remove('deal_bids')
            if 'id' in cols:
                cols.remove('id')
            col = [i for i in cols if i not in ['dsp_id', 'ssp_id', 'deal_id', 'date']][0]
            print(col)
            data = pandas.read_csv(os.path.dirname(os.path.realpath(__file__))+"/tmp/video/{0}.csv".format(col))
            if t == str:
                data[col] = data[col].fillna('')
            data[col] = data[col].astype(t)
            data['date'] = data['date'].apply(lambda x: datetime.datetime.strptime(x[:10], '%Y-%m-%d'))
            data = data.to_dict(orient='records')
            items = []
            for item in data:
                items.append(model(**item))
            try:
                model.objects.bulk_create(items)
            except:
                continue
        for query, model, t in banner.QUERIES:
            # model.objects.all().delete()
            cols = [i.name for i in list(model._meta.get_fields())]
            cols.remove('requests')
            cols.remove('had_deal_response')
            cols.remove('deal_bids')
            if 'id' in cols:
                cols.remove('id')
            col = [i for i in cols if i not in ['dsp_id', 'ssp_id', 'deal_id', 'date']][0]
            print(col)
            data = pandas.read_csv(os.path.dirname(os.path.realpath(__file__))+"/tmp/banner/{0}.csv".format(col))
            if t == str:
                data[col] = data[col].fillna('')
            data[col] = data[col].fillna(-1).astype(t)
            data['date'] = data['date'].apply(lambda x: datetime.datetime.strptime(x[:10], '%Y-%m-%d'))
            data = data.to_dict(orient='records')
            items = []
            for item in data:
                items.append(model(**item))
            try:
                model.objects.bulk_create(items)
            except:
                continue
