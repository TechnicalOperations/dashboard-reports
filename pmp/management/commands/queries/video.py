import datetime
from pmp.models import VideoApi, VideoBattr, VideoSize, VideoMaxDuration, VideoMinDuration, VideoMimes, VideoPos, VideoProtocols


QUERIES = []


end = datetime.datetime.now()
end = end - datetime.timedelta(hours=end.hour)
start = end - datetime.timedelta(days=1)

start = start.strftime("%Y-%m-%d")
end = end.strftime("%Y-%m-%d")


ARRAY_QUERY = """
    SELECT DATE_TRUNC('DAY', A.event_time) AS date,
           C.value::varchar AS deal_id,
           A.ssp_id,
           A.dsp_id,
           ARRAY_COMPACT(A.request_data:imp[0]:video:{field_ortb}) AS {field},
           COUNT(*) AS requests,
           COUNT(B.deal_id) AS had_deal_response,
           SUM(IFF(CONTAINS(C.value::varchar, B.deal_id), 1, 0)) AS deal_bids
    FROM rx.dspbidrequest AS A
    LEFT JOIN rx.bidresponse AS B ON A.request_id=B.request_id
                              AND A.dsp_id=B.dsp_id,
    LATERAL FLATTEN(input=>PARSE_JSON(A.rx_deal_ids)) C
    WHERE A.event_time >= '{start}' AND A.event_time < '{end}'
        AND A.rx_deal_ids IS NOT NULL AND ARRAY_SIZE(PARSE_JSON(A.rx_deal_ids)) > 0
    GROUP BY date, C.value, A.ssp_id, A.dsp_id, {field}
"""


VIDEO_API = ARRAY_QUERY.format(start=start, end=end, field_ortb='api', field='api')

VIDEO_BATTR = ARRAY_QUERY.format(start=start, end=end, field_ortb='battr', field='battr')

VIDEO_SIZE = """
    SELECT DATE_TRUNC('DAY', A.event_time) AS date,
            D.value::varchar AS deal_id,
            A.dsp_id,
            A.ssp_id,
            A.request_data:imp[0]:video:w::varchar || 'x' || A.request_data:imp[0]:video:h::varchar AS size,
            COUNT(*) AS requests,
            COUNT(B.deal_id) AS had_deal_response,
            SUM(IFF(CONTAINS(D.value, B.deal_id), 1, 0)) AS deal_bids
    FROM rx.dspbidrequest AS A
    LEFT JOIN rx.bidresponse AS B ON A.request_id=B.request_id AND A.dsp_id=B.dsp_id,
    LATERAL FLATTEN(input => ARRAY_COMPACT(PARSE_JSON(A.rx_deal_ids))) D
    WHERE A.event_time >= '{start}' AND A.event_time < '{end}' AND A.request_data:imp[0]:video IS NOT NULL
    GROUP BY date, D.value, A.dsp_id, A.ssp_id, size
""".format(start=start, end=end)

VIDEO_MAXDURATION = """
    SELECT DATE_TRUNC('DAY', A.event_time) AS date,
            D.value::varchar AS deal_id,
            A.dsp_id,
            A.ssp_id,
            A.request_data:imp[0]:video:maxduration::varchar AS maxduration,
            COUNT(*) AS requests,
            COUNT(B.deal_id) AS had_deal_response,
            SUM(IFF(D.value = B.deal_id, 1, 0)) AS deal_bids
    FROM rx.dspbidrequest AS A
    LEFT JOIN rx.bidresponse AS B ON A.request_id=B.request_id AND A.dsp_id=B.dsp_id,
    LATERAL FLATTEN(input => ARRAY_COMPACT(PARSE_JSON(A.rx_deal_ids))) D
    WHERE A.event_time >= '{start}' AND A.event_time < '{end}' AND A.request_data:imp[0]:video IS NOT NULL
    GROUP BY date, D.value, A.dsp_id, A.ssp_id, maxduration
""".format(start=start, end=end)

VIDEO_MINDURATION = """
    SELECT DATE_TRUNC('DAY', A.event_time) AS date,
            D.value::varchar AS deal_id,
            A.dsp_id,
            A.ssp_id,
            A.request_data:imp[0]:video:minduration::varchar AS minduration,
            COUNT(*) AS requests,
            COUNT(B.deal_id) AS had_deal_response,
            SUM(IFF(D.value = B.deal_id, 1, 0)) AS deal_bids
    FROM rx.dspbidrequest AS A
    LEFT JOIN rx.bidresponse AS B ON A.request_id=B.request_id AND A.dsp_id=B.dsp_id,
    LATERAL FLATTEN(input => ARRAY_COMPACT(PARSE_JSON(A.rx_deal_ids))) D
    WHERE A.event_time >= '{start}' AND A.event_time < '{end}' AND A.request_data:imp[0]:video IS NOT NULL
    GROUP BY date, D.value, A.dsp_id, A.ssp_id, minduration
""".format(start=start, end=end)

VIDEO_MIMES = ARRAY_QUERY.format(start=start, end=end, field_ortb='mimes', field='mime')

VIDEO_POS = """
    SELECT DATE_TRUNC('DAY', A.event_time) AS date,
            D.value::varchar AS deal_id,
            A.dsp_id,
            A.ssp_id,
            A.request_data:imp[0]:video:pos::varchar AS pos,
            COUNT(*) AS requests,
            COUNT(B.deal_id) AS had_deal_response,
            SUM(IFF(D.value = B.deal_id, 1, 0)) AS deal_bids
    FROM rx.dspbidrequest AS A
    LEFT JOIN rx.bidresponse AS B ON A.request_id=B.request_id AND A.dsp_id=B.dsp_id,
    LATERAL FLATTEN(input => ARRAY_COMPACT(PARSE_JSON(A.rx_deal_ids))) D
    WHERE A.event_time >= '{start}' AND A.event_time < '{end}' AND A.request_data:imp[0]:video IS NOT NULL
    GROUP BY date, D.value, A.dsp_id, A.ssp_id, pos
""".format(start=start, end=end)

VIDEO_PROTOCOLS = ARRAY_QUERY.format(start=start, end=end, field_ortb='protocols', field='protocol')


QUERIES.append((VIDEO_API, VideoApi, str,))
QUERIES.append((VIDEO_BATTR, VideoBattr, str,))
QUERIES.append((VIDEO_SIZE, VideoSize, str,))
QUERIES.append((VIDEO_MAXDURATION, VideoMaxDuration, int,))
QUERIES.append((VIDEO_MINDURATION, VideoMinDuration, int,))
QUERIES.append((VIDEO_MIMES, VideoMimes, str,))
QUERIES.append((VIDEO_POS, VideoPos, int,))
QUERIES.append((VIDEO_PROTOCOLS, VideoProtocols, str,))
