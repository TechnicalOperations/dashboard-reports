import os
import json
import pandas
pandas.set_option('display.width', 5000)

fs = os.listdir()
fs = [i for i in fs if '.csv' in i]

for f in fs:
	data = pandas.read_csv(f)
	col = f.split('.')[0]
	try:
		data[col] = data[col].apply(lambda x: [] if type(x) != str else json.loads(x))
		data[col] = data[col].apply(lambda x: tuple(sorted(list(set([str(i) for i in x])))))
		data[col] = data[col].apply(lambda x: ','.join(x))
		cols = list(data.columns)
		cols.remove('date')
		cols.remove('dsp_id')
		cols.remove('ssp_id')
		cols.remove('deal_id')
		cols.remove(col)
		data.to_csv(f, index=False, sep=',')
	except:
		continue

