from django.db import models
from django.utils import timezone


class Deals(models.Model):
    deal_id = models.CharField(max_length=255, blank=False, null=False, default='')
    description = models.CharField(max_length=255, blank=False, null=False, default='')


class Aggregates(models.Model):
    date = models.DateField(default=timezone.now)
    dsp_id = models.CharField(max_length=50, blank=False, null=False, default='UNKNOWN')
    ssp_id = models.CharField(max_length=50, blank=False, null=False, default='UNKNOWN')
    deal_id = models.CharField(max_length=255, blank=False, null=False, default='None', db_index=True)
    requests = models.IntegerField(default=0)
    had_deal_response = models.IntegerField(default=0)
    deal_bids = models.IntegerField(default=0)

    class Meta:
        abstract = True


class VideoApi(Aggregates):
    api = models.CharField(max_length=255, blank=True, null=True, default=None)


class VideoBattr(Aggregates):
    battr = models.CharField(max_length=255, blank=True, null=True, default=None)


class VideoSize(Aggregates):
    size = models.CharField(max_length=20, blank=True, null=True, default=None)


class VideoMaxDuration(Aggregates):
    maxduration = models.IntegerField(blank=True, null=True, default=None)


class VideoMinDuration(Aggregates):
    minduration = models.IntegerField(blank=True, null=True, default=None)


class VideoMimes(Aggregates):
    mime = models.TextField(blank=True, null=True, default=None)


class VideoPos(Aggregates):
    pos = models.IntegerField(blank=True, null=True, default=None)


class VideoProtocols(Aggregates):
    protocol = models.CharField(max_length=255, blank=True, null=True, default=None)


class VideoBidSummary(models.Model):
    deal_bid = models.BooleanField(default=False)
    deal_id = models.CharField(max_length=255, default='')
    dsp_id = models.CharField(max_length=50, default='')
    api = models.CharField(max_length=255, blank=True, null=True, default=None)
    battr = models.CharField(max_length=255, blank=True, null=True, default=None)
    size = models.TextField(default='')
    maxduration = models.CharField(max_length=255, blank=True, null=True, default=None)
    minduration = models.CharField(max_length=255, blank=True, null=True, default=None)
    mime = models.CharField(max_length=100, blank=True, null=True, default=None)
    pos = models.CharField(max_length=255, blank=True, null=True, default=None)
    protocol = models.CharField(max_length=255, blank=True, null=True, default=None)


class BannerApi(Aggregates):
    api = models.CharField(max_length=255, blank=True, null=True, default=None)


class BannerBattr(Aggregates):
    battr = models.CharField(max_length=255, blank=True, null=True, default=None)


class BannerSize(Aggregates):
    size = models.CharField(max_length=20, blank=True, null=True, default=None)


class BannerMimes(Aggregates):
    mime = models.CharField(max_length=100, blank=True, null=True, default=None)


class BannerPos(Aggregates):
    pos = models.IntegerField(blank=True, null=True, default=None)


class BannerBidSummary(models.Model):
    deal_bid = models.BooleanField(default=False)
    deal_id = models.CharField(max_length=255, default='')
    dsp_id = models.CharField(max_length=50, default='')
    api = models.CharField(max_length=255, blank=True, null=True, default=None)
    battr = models.CharField(max_length=255, blank=True, null=True, default=None)
    size = models.TextField(default='')
    mime = models.CharField(max_length=100, blank=True, null=True, default=None)
    pos = models.CharField(max_length=255, blank=True, null=True, default=None)
