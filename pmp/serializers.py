from datetime import datetime, date
from rest_framework import serializers


class PmpChartSerializer(serializers.Serializer):

    CHART_COLORS = [
        'rgba(0, 0, 255, 1)',
        'rgba(0, 255, 0, 1)',
        'rgba(255, 0, 0, 1)',
    ]

    COLUMNS = {
        'api',
        'battr',
        'size',
        'maxduration',
        'minduration',
        'mime',
        'pos',
        'protocol'
    }

    def color_iterator(self):
        return iter(PmpChartSerializer.CHART_COLORS)

    def to_representation(self, instance):
        keys = list(instance.keys())
        keys.remove('date')
        out = []
        colors = self.color_iterator()
        for key in keys:
            out.append({
                'label': key,
                'data': [v for v in instance[key].values()],
                'backgroundColor': 'rgba(0, 0, 0, 0)',
                'borderColor': next(colors),
                'borderWidth': 1
            })
        out = {
            'labels': [int(v.timestamp() * 1000) for v in instance['date'].values()],
            'datasets': out
        }
        return out
