function includeCsrfInRequests($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
}

var app = angular.module('pmpApp', []).config(includeCsrfInRequests);

app.controller('pmpController', function($scope, $http) {
    $scope.dsp = null;
    $scope.charts = [];

    $scope.filterOwner = function() {
        window.location = "/pmp/"+$scope.owner+"/";
    }

    $scope.plotCharts = function() {
        $http.post(window.location.pathname, {dsp_id: $scope.dsp})
            .then(function(response) {
                $scope.data = response.data;
                for (var index = 0; index < $scope.charts.length; index++) {
                    $scope.charts[index].clear();
                    $scope.charts[index].destroy();
                }
                $scope.charts = [];
                console.dir($scope.data)
                var context;
                var parent;
                var item;
                for (var index = 0; index < $scope.data.length; index++) {
                    item = $scope.data[index];
                    context = document.getElementById("bids-"+item.key)
                    context = context.getContext("2d");
                    $scope.charts.push(new Chart(context, {
                        type: 'bar',
                        data: {
                            labels: item.labels,
                            datasets: [{
                                label: 'Bids',
                                data: item.values.bids,
                                backgroundColor: 'rgba(0, 255, 0, 1)',
                                borderColor: 'rgba(0, 255, 0, 1)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            legend: {
                                position: 'left'
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    }));
                    context = document.getElementById("nobids-"+item.key)
                    context = context.getContext("2d");
                    $scope.charts.push(new Chart(context, {
                        type: 'bar',
                        data: {
                            labels: item.labels,
                            datasets: [{
                                label: 'No Bids',
                                data: item.values.nobids,
                                backgroundColor: 'rgba(0, 0, 255, 1)',
                                borderColor: 'rgba(0, 0, 255, 1)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            legend: {
                                position: 'left'
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    }));
                }
                console.log("Successfully fetched filter JSON");
            }, function(response) {
                console.log("Error getting filter JSON");
            });
        }
        $scope.plotCharts();

});
