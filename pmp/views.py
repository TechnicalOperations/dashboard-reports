import pandas
from django.shortcuts import render, HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from .attribute_map import ATTRIBUTES, INVERT
from django.db import connection
from pmp import models
from pmp import serializers
from copy import deepcopy
import os
pandas.set_option('display.width', 5000)


OWNERS = pandas.read_csv('./pmp/deals_v_owners.csv')
OWNERS = OWNERS.to_dict(orient='records')
OWNERS = {i['deal_id']: (i['owner'], i['description'],) for i in OWNERS}
NLENGTH = list(set([len(k) for k in OWNERS]))[0] - 3


DEAL_DSP_QUERY = """SELECT deal_id, dsp_id FROM (SELECT deal_id, dsp_id FROM pmp_videoapi
UNION
SELECT deal_id, dsp_id FROM pmp_videobattr
UNION
SELECT deal_id, dsp_id FROM pmp_videosize
UNION
SELECT deal_id, dsp_id FROM pmp_videomaxduration
UNION
SELECT deal_id, dsp_id FROM pmp_videominduration
UNION
SELECT deal_id, dsp_id FROM pmp_videomimes
UNION
SELECT deal_id, dsp_id FROM pmp_videopos
UNION
SELECT deal_id, dsp_id FROM pmp_videoprotocols
UNION
SELECT deal_id, dsp_id FROM pmp_bannerapi
UNION
SELECT deal_id, dsp_id FROM pmp_bannerbattr
UNION
SELECT deal_id, dsp_id FROM pmp_bannersize
UNION
SELECT deal_id, dsp_id FROM pmp_bannermimes
UNION
SELECT deal_id, dsp_id FROM pmp_bannerpos) AS A GROUP BY deal_id, dsp_id"""


def flatten(data, key):
    data = data.to_dict(orient='records')
    out = []
    for i in data:
        if len(i[key]) == 0:
            i[key] = ''
            out.append(i)
        for k in list(set(i[key])):
            t = deepcopy(i)
            t[key] = k
            out.append(t)
    return pandas.DataFrame(out)


# Create your views here.
class Index(APIView):

    AGG_COLS = ['dsp_id', 'deal_id']
    SUM_COLS = ['requests', 'had_deal_response', 'deal_bids']

    VIDEO_FILTERS = {
        'api': (models.VideoApi, 'api',),
        'battr': (models.VideoBattr, 'battr',),
        'size': (models.VideoSize, 'size',),
        'maxduration': (models.VideoMaxDuration, 'maxduration',),
        'minduration': (models.VideoMinDuration, 'minduration',),
        'mime': (models.VideoMimes, 'mime',),
        'pos': (models.VideoPos, 'pos',),
        'protocol': (models.VideoProtocols, 'protocol',)
    }

    BANNER_FILTERS = {
        'api': (models.BannerApi, 'api',),
        'battr': (models.BannerBattr, 'battr',),
        'size': (models.BannerSize, 'size',),
        'mime': (models.BannerMimes, 'mime',),
        'pos': (models.BannerPos, 'pos',),
    }

    def create_data(self, deal, dsp_id=None):
        data = []
        totals = {}
        lists = ['battr', 'protocol', 'mime', 'api']
        for key, model in list(Index.VIDEO_FILTERS.items())+list(Index.BANNER_FILTERS.items()):
            tmp = model[0].objects.filter(deal_id=deal)
            tmp = tmp if dsp_id is None else tmp.filter(dsp_id=dsp_id)
            tmp = pandas.DataFrame([
                {
                    'dsp_id': item.dsp_id,
                    'key': key,
                    'attribute': getattr(item, key),
                    'bid_different_deal': item.had_deal_response,
                    'requests': item.requests,
                    'deal_bids': item.deal_bids
                }
                for item in tmp
            ])
            if len(tmp) == 0:
                continue
            totals[key] = tmp['requests'].sum()
            if key in lists:
                tmp['attribute'] = tmp['attribute'].apply(lambda x: x.split(',') if type(x) == str else '')
                tmp['attribute'] = flatten(tmp, 'attribute')
            data.append(tmp)
        data = pandas.concat(data).reset_index(drop=True)
        del data['dsp_id']
        data = data.groupby(['key', 'attribute']).sum().reset_index()
        data['attribute'] = data.apply(
            lambda x: ATTRIBUTES[x['key']].get(str(x['attribute']), 'None') if x['key'] in ATTRIBUTES else x['attribute'],
            axis=1
        )
        # data['total'] = data['key'].apply(lambda x: totals[x])
        data['total'] = data['requests']
        data['bids_count'] = data['deal_bids']
        data['bids_percent'] = data['deal_bids'] * 100.0 / data['total']
        data['percent_other_deal_bids'] = data['bid_different_deal'] * 100.0 / data['total']
        data['nobids_count'] = data['requests'] - data['deal_bids']
        data['nobids_percent'] = data['nobids_count'] * 100.0 / data['total']

        return data

    def get(self, request, owner=None, deal=None):
        with connection.cursor() as cursor:
            cursor.execute(DEAL_DSP_QUERY)
            data = cursor.fetchall()
            data = [{'deal_id': i[0], 'dsp_id': i[1]} for i in data]
        out = {
            'deals': sorted(list(set([(i['deal_id'], OWNERS.get(i['deal_id'], [None, 'Unknown'])[1],) for i in data]))),
            'dsps': sorted(list(set([i['dsp_id'] for i in data if i['deal_id'] == deal or deal is None]))),
            'owner': owner,
            'owners': sorted(list(set([i[0] for i in OWNERS.values()])))
        }
        out['deals'] = [(i[0], "..."+i[1][len(i[1]) - NLENGTH:],) for i in out['deals']]
        if owner is not None:
            out['deals'] = [i for i in out['deals'] if i[0] in [k for k, v in OWNERS.items() if v[0] == owner]]
        if deal is None:
            return render(request, 'pmp/index.html', out)
        out['deal'] = deal
        out['data'] = self.create_data(deal).to_dict(orient='records')
        return render(request, 'pmp/index.html', out)

    def post(self, request, owner=None, deal=None):
        if deal is None:
            return HttpResponse(status=204)
        data = self.create_data(deal, **request.data)
        attrs = list(set(data['key']))
        out = []
        for attr in attrs:
            tmp = data[(data['key'] == attr)]
            tmp = tmp.sort_values(by=['nobids_percent', 'bids_percent'], ascending=[False, False])
            top = tmp[:5].copy().reset_index(drop=True)
            bot = tmp[5:].copy().reset_index(drop=True)
            bot['attribute'] = 'Other'
            bot = bot.groupby(['key', 'attribute']).sum().reset_index()
            bot['bids_percent'] = bot['deal_bids'] * 100.0 / bot['total']
            bot['nobids_percent'] = bot['nobids_count'] * 100.0 / bot['total']
            tmp = pandas.concat([top, bot])
            out.append({
                'key': attr,
                'labels': list(tmp['attribute']),
                'values': {
                    'nobids': list(tmp['nobids_percent']),
                    'bids': list(tmp['bids_percent'])
                }
            })
        return Response(out)
