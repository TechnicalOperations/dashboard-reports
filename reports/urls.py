"""pmp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from index.views import Index, FilterView, ChartView, Generic
from pmp import views
from filters import views as filter_views

pmp_urls = [
    url(r'^(?P<owner>[a-zA-Z ]+)/(?P<deal>[a-zA-Z\-0-9]+)/', views.Index.as_view()),
    url(r'^(?P<owner>[a-zA-Z ]+)/', views.Index.as_view()),
    url(r'^(?P<deal>[a-zA-Z\-0-9]+)/', views.Index.as_view()),
    url(r'^', views.Index.as_view())
]

generic_urls = [
    url(r'^(?P<class_view>[a-zA-Z0-9\-]+)/filters/', FilterView.as_view()),
    url(r'^(?P<class_view>[a-zA-Z0-9\-]+)/get/', ChartView.as_view()),
    url(r'^', Generic.as_view())
]

funnel_urls = [
    url(r'^search/(?P<text>.*?)/', filter_views.text_completion),
    url(r'^(?P<deal_id>[a-zA-Z0-9\-]+)/', filter_views.index),
    url(r'^', filter_views.index)
]

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^pmp/', include(pmp_urls)),
    url(r'^gen/', include(generic_urls)),
    url(r'^pmp-funnel/', include(funnel_urls)),
    url(r'^', Index.as_view())
]
